package com.amazon.features.browser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.amazon.steps.serenity.HomeSteps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class WhenOpeningWeb {

	@Managed
	WebDriver driver;
	
	@Steps
	HomeSteps thao;
	
	@Test
	public void launch_web_application() {
		thao.open_application();
	}
}
