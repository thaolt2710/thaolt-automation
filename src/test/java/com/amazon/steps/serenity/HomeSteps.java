package com.amazon.steps.serenity;

import com.amazon.pages.HomePage;

import net.thucydides.core.annotations.Step;

public class HomeSteps {
	HomePage onHomePage;
	
	@Step
	public void open_application() {
		onHomePage.open();
	}
}
